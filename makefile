all: BatalhaNaval

BatalhaNaval: main.o funcoes.o
	gcc -o BatalhaNaval main.o funcoes.o

main.o: main.c BatalhaNaval.h
	gcc -o main.o main.c -c

funcoes.o: funcoes.c BatalhaNaval.h
	gcc -o funcoes.o funcoes.c -c

clean:
	rm -rfv  *.o
