#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#include <string.h>
#include <unistd.h>
#include <ctype.h>

#define MAR CIANO"~ ~ ~ "COR_RESET
#define NAVIO COR_RESET"000000"COR_RESET
#define AFUNDADO VERMELHO"XXXXXX"COR_RESET
#define TIRONAAGUA "      "
#define QNTPA /*quantidade de porta aviões */ 1
#define TAM 8
#define TAMPA /*Tamanho dos porta aviões*/ 4
#define QNTEN /*quantidade encouraçados*/ 2
#define TAMEN /*tamanho encouraçado*/ 3
#define QNTCR /*quantidade de cruzadores*/ 2
#define TAMCR /*tamanho cruzador*/ 2
#define QNTSUB /*quantidade de submarinos*/ 4
#define TAMSUB /*tamanho submarinos*/ 1
/* MACROS para as cores */
#define MAGENTA "\x1b[35m"
#define CIANO    "\x1b[36m"
#define VERDE   "\x1b[32m"
#define VERMELHO "\x1b[31m"
#define COR_RESET   "\x1b[0m"


/* Definição do pequeno Struct Usuário que armazenará o seu nome e sexo */
struct _PLAYER {
    char* nome;
    char* sexo;
} PLAYER;




/* Protótipos das funções */
int titulo_in(void);
void introducao(void);
char* valida_string(int parametro);
void gera_tabuleiro(short int * ponteiro_para_tabuleiro);
void imprime_tabuleiro(short int * ponteiro_para_tabuleiro);
void monta_tabuleiro_jogador(short int * ponteiro_para_tabuleiro);
void muda_valor(short int * ponteiro_para_tabuleiro,int alvo);
short int recebe_coordenadas();
int verifica_embarcacao(short int primeira_coordenada,short int ultima_coordenada,short int tamanho_da_embarcacao);
int cria_embarcacao(short int * ponteiro_para_tabuleiro,int primeira_coordenada,int ultima_coordenada, int tamanho_da_embarcacao);
int cria_submarino(short int * ponteiro_para_tabuleiro,int coordenada);
void coloca_submarinos(short int * ponteiro_para_tabuleiro);
void monta_tabuleiro_auto(short int * ponteiro_para_tabuleiro);
int tiro_jogador(short int * ponteiro_para_tabuleiro);
int tiro_aleatorio_computador(short int * ponteiro_para_tabuleiro,short int * ultimo_ataque);
int ataque_computador(short int * ponteiro_para_tabuleiro,short int * ultimo_ataque,short int * sucesso_ultimo_ataque,short int * sentido_direcao);
int testa_fim(short int * ponteiro_para_tabuleiro);
int retorna_ponteiro(short int * ponteiro_para_tabuleiro,int alvo);
void imprime_tabuleiro_computador(short int * ponteiro_para_tabuleiro);
