#include "BatalhaNaval.h"

/*
========================================================
                    FUNÇÕES GERAIS                       */
void imprime_tabuleiro(short int * ponteiro_para_tabuleiro) {
    int i,j;
    char * imprime;

    printf("      A      B      C      D      E      F      G      H\n\n");
    /* este for vai percorrer toda o vetor tabuleiro 3 vezes para que ele possa ser printado
    segundo o modelo de tabuleiro, Fileira para esse programa são 8 colunas consecutivas por uma linha.
    ou seja uma matriz de 3x8, onde se repetem 3 vezes cada coordenada */
    for(i = 0; i < TAM * TAM * 3; i++, ponteiro_para_tabuleiro++){
        /*O valor que o ponteiro estiver apontando vai mudar o ponteiro de impressão para a string
        adequada*/
        if(*ponteiro_para_tabuleiro == 0) imprime = MAR;
        else if(*ponteiro_para_tabuleiro == 2) imprime = AFUNDADO;
        else if(*ponteiro_para_tabuleiro == 1) imprime = NAVIO;
        else if(*ponteiro_para_tabuleiro == 3) imprime = TIRONAAGUA;

        switch(i%24){
            /* Quando estiver na primeira coordenada da fileira vai printar o separador de fileiras, em seguida
            printa o printavel do valor da coordenada */
            case 0:
                printf(VERDE"\n   +------+------+------+------+------+------+------+------+\n"COR_RESET VERDE"   |"COR_RESET"%s"VERDE"|"COR_RESET,imprime);
                break;
            case 7:
                printf("%s"VERDE"|"COR_RESET,imprime);
                ponteiro_para_tabuleiro -= TAM;
                break;
                /* quando estiver na ultima coordenada da segunda linha da fileira printa o valor da coordenada
                e corrige ponteiro para voltar para a primeira coordenada */
            case 15:
                printf("%s"VERDE"|"COR_RESET,imprime);
                ponteiro_para_tabuleiro -= TAM;
                break;
                /* Corrige quando estiver na linha do meio da fileira printa o valor da fileira antes de
                printar o imprime da coordenada */
            case 8:
                printf("\n%d  "VERDE"|"COR_RESET"%s"VERDE"|"COR_RESET,(i/24)+1,imprime);
                break;
            case 16:
                printf("\n   "VERDE"|"COR_RESET"%s"VERDE"|"COR_RESET,imprime);
                break;
            default:
                /* quando estiver na última coordenada da primeira linha da fileira printa o valor da coordenada */
                printf("%s"VERDE"|"COR_RESET,imprime);
        }
    }
    printf(VERDE"\n   +------+------+------+------+------+------+------+------+\n"COR_RESET);
}

void imprime_tabuleiro_computador(short int * ponteiro_para_tabuleiro) {
    int i,j;
    char * imprime;

    printf("      A      B      C      D      E      F      G      H\n\n");
    /* este for vai percorrer toda o vetor tabuleiro 3 vezes para que ele possa ser printado
    segundo o modelo de tabuleiro, Fileira para esse programa são 8 colunas consecutivas por uma linha.
    ou seja uma matriz de 3x8, onde se repetem 3 vezes cada coordenada */
    for(i = 0; i < TAM * TAM * 3; i++, ponteiro_para_tabuleiro++){
        /* O valor que o ponteiro estiver apontando vai mudar o ponteiro de impressão para a string
        adequada */
        if(*ponteiro_para_tabuleiro == 0) imprime = MAR;
        else if(*ponteiro_para_tabuleiro == 2) imprime = AFUNDADO;
        else if(*ponteiro_para_tabuleiro == 1) imprime = MAR;
        else if(*ponteiro_para_tabuleiro == 3) imprime = TIRONAAGUA;

        switch(i%24){
            /* Quando estiver na primeira coordenada da fileira vai printar o separador de fileiras, em seguida
            printa o printavel do valor da coordenada */
            case 0:
                printf(VERDE"\n   +------+------+------+------+------+------+------+------+\n"COR_RESET VERDE"   |"COR_RESET"%s"VERDE"|"COR_RESET,imprime);
                break;
            case 7:
                printf("%s"VERDE"|"COR_RESET,imprime);
                ponteiro_para_tabuleiro -= TAM;
                break;
                /* quando estiver na ultima coordenada da segunda linha da fileira printa o valor da coordenada
                e corrige ponteiro para voltar para a primeira coordenada */
            case 15:
                printf("%s"VERDE"|"COR_RESET,imprime);
                ponteiro_para_tabuleiro -= TAM;
                break;
                /* Corrige quando estiver na linha do meio da fileira printa o valor da fileira antes de
                printar o imprime da coordenada */
            case 8:
                printf("\n%d  "VERDE"|"COR_RESET"%s"VERDE"|"COR_RESET,(i/24)+1,imprime);
                break;
            case 16:
                printf("\n   "VERDE"|"COR_RESET"%s"VERDE"|"COR_RESET,imprime);
                break;
            default:
                /* quando estiver na última coordenada da primeira linha da fileira printa o valor da coordenada */
                printf("%s"VERDE"|"COR_RESET,imprime);
        }
    }
    printf(VERDE"\n   +------+------+------+------+------+------+------+------+\n"COR_RESET);
}

short int recebe_coordenadas() {
    /* Função recebe entrada pelo usuário pelo teclado, valida e retorna a posição dela no tabuleiro */
    static int quantas_vezes_usada = 0;
    char entrada[3];
    char caracteres_validos[TAM*2]= {'A','a','B','b','C','c','D','d','E','e','F','f','G','g','H','h'};
    int  valor = 0,i; /* validador para entrada correta e contador */
    int erro = 0; /* caso entrada seja errada muda mensagem pro usuário */
    /* Looping apenas se encerra quando valor for validado para a letra e
    numero da coordenada */
    int valor_linha,valor_coluna;
    while(valor != 2){
        valor = 0; /* reinicia valor */
        /* evita repetições desnecessárias */
        if(quantas_vezes_usada == 0 && erro == 0) {
            printf("Deve falar primeiro a coordenada da coluna (de A à H) seguida de um número (1 à 8)\n");
        }
        else if (erro > 0){
            printf("Coordenada invalida, por favor me diga uma letra e um número\n");
        }
        else{
            printf("Diga a coordenada por favor.\n");
        }

        fgets(entrada, 3, stdin);
        getchar();
        /* validação da coluna da coordenada */
        for(i = 0; i < TAM * 2; i++){
            if(entrada[0] == caracteres_validos[i]) {
                valor++;
                break;
            }
            erro++;
        }
        valor_linha = entrada[1] - '1'; /* da a coordenada da linha de 0 à 7 */
        if(valor_linha < TAM && valor_linha >=0) {
            valor++;
        }
        else{
            erro++;
        }
    }
    /* usa a tabella ascii para colocar o valor de a = 0 até h = 7 */
    valor_coluna = entrada[0] - 'a';
    if(valor_coluna < 0){
        valor_coluna += 32;
    }
    /* a posição na fita pode ser pensada como uma tabela de divisão onde a linha
    representa o quociente e a coluna o resto, logo a posição será dada pela sentença abaixo */
    quantas_vezes_usada++;
    return ((valor_linha*TAM)+valor_coluna);
}

void muda_valor(short int * ponteiro_para_tabuleiro,int alvo) {
    /* muda valor de no de mar para navio, ou de navio para afundado */
    ponteiro_para_tabuleiro += alvo; /* aponta para alvo */
    (*ponteiro_para_tabuleiro)++; /* muda seu valor */
}

int retorna_valor_tabuleiro(short int * ponteiro_para_tabuleiro,int alvo) {
    ponteiro_para_tabuleiro += alvo;
    return *ponteiro_para_tabuleiro;
}
/*
========================================================
          FUNÇÕES DE INICIO DE JOGO                      */

int titulo_in(void) { /* Função responsável por printar o título do jogo */

    int i;

    for(i = 0; i < 210; i++) {

        usleep(1000);
        printf(MAGENTA"-");

    }
    printf("\n");

    /*Tentamos fazer um for para não ter o trabalho braçal de mudar as cores manualmente, contudo, tivemos problemas com os caracteres especiais e, por isso, optamos pelo método abaixo:*/

    usleep(100000);
    printf(CIANO"                                                  ██████"VERDE"╗"CIANO"  █████"VERDE"╗"CIANO" ████████"VERDE"╗"CIANO" █████"VERDE"╗"CIANO" ██"VERDE"╗"CIANO"     ██"VERDE"╗"CIANO"  ██"VERDE"╗"CIANO" █████"VERDE"╗"CIANO"     ███"VERDE"╗"CIANO"   ██"VERDE"╗"CIANO" █████"VERDE"╗"CIANO" ██"VERDE"╗"CIANO"   ██"VERDE"╗"CIANO" █████"VERDE"╗"CIANO" ██"VERDE"╗""\n");
    usleep(100000);
    printf(CIANO"                                                  ██"VERDE"╔══"CIANO"██"VERDE"╗"CIANO"██"VERDE"╔══"CIANO"██"VERDE"╗╚══"CIANO"██"VERDE"╔══╝"CIANO"██"VERDE"╔══"CIANO"██"VERDE"╗"CIANO"██"VERDE"║"CIANO"     ██"VERDE"║"CIANO"  ██"VERDE"║"CIANO"██"VERDE"╔══"CIANO"██"VERDE"╗"CIANO"    ████"VERDE"╗"CIANO"  ██"VERDE"║"CIANO"██"VERDE"╔══"CIANO"██"VERDE"╗"CIANO"██"VERDE"║"CIANO"   ██"VERDE"║"CIANO"██"VERDE"╔══"CIANO"██"VERDE"╗"CIANO"██"VERDE"║""\n");
    usleep(100000);
    printf(CIANO"                                                  ██████"VERDE"╔╝"CIANO"███████"VERDE"║"CIANO"   ██"VERDE"║"CIANO"   ███████"VERDE"║"CIANO"██"VERDE"║"CIANO"     ███████"VERDE"║"CIANO"███████"VERDE"║"CIANO"    ██"VERDE"╔"CIANO"██"VERDE"╗" CIANO" ██"VERDE"║"CIANO"███████"VERDE"║"CIANO"██"VERDE"║"CIANO"   ██"VERDE"║"CIANO"███████"VERDE"║"CIANO"██"VERDE"║""\n");
    usleep(100000);
    printf(CIANO"                                                  ██"VERDE"╔══"CIANO"██"VERDE"╗"CIANO"██"VERDE"╔══"CIANO"██"VERDE"║"CIANO"   ██"VERDE"║"CIANO"   ██"VERDE"╔══"CIANO"██"VERDE"║"CIANO"██"VERDE"║"CIANO"     ██"VERDE"╔══"CIANO"██"VERDE"║"CIANO"██"VERDE"╔══"CIANO"██"VERDE"║"CIANO"    ██"VERDE"║╚"CIANO"██"VERDE"╗"CIANO"██"VERDE"║"CIANO"██"VERDE"╔══"CIANO"██"VERDE"║╚"CIANO"██"VERDE"╗"CIANO" ██"VERDE"╔╝"CIANO"██"VERDE"╔══"CIANO"██"VERDE"║"CIANO"██"VERDE"║""\n");
    usleep(100000);
    printf(CIANO"                                                  ██████"VERDE"╔╝"CIANO"██"VERDE"║"CIANO"  ██"VERDE"║"CIANO"   ██"VERDE"║"CIANO"   ██"VERDE"║"CIANO"  ██"VERDE"║"CIANO"███████"VERDE"╗"CIANO"██"VERDE"║"CIANO"  ██"VERDE"║"CIANO"██"VERDE"║"CIANO"  ██"VERDE"║"CIANO"    ██"VERDE"║ ╚"CIANO"████"VERDE"║"CIANO"██"VERDE"║"CIANO"  ██"VERDE"║ ╚"CIANO"████"VERDE"╔╝"CIANO" ██"VERDE"║"CIANO""CIANO"  ██"VERDE"║"CIANO"███████"VERDE"╗""\n");
    usleep(100000);
    printf(VERDE"                                                  ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚═╝  ╚═══╝╚═╝  ╚═╝  ╚═══╝  ╚═╝  ╚═╝╚══════╝\n");

    for(i = 0; i < 210; i++) {

        usleep(1000);
        printf(MAGENTA"-");

    }
    printf("\n");

    printf(VERMELHO"                                              /\\         | (_)               | |                   | |        / ____|                       | |\n");
    printf(VERMELHO"                                             /  \\      __| |_ ___ _ __  _   _| |_ __ _   _ __   ___| | __ _  | |  __ _   _  __ _ _ __   __ _| |__   __ _ _ __ __ _ \n");
    printf(VERMELHO"                                            / /\\ \\    / _` | / __| '_ \\| | | | __/ _` | | '_ \\ / _ \\ |/ _` | | | |_ | | | |/ _` | '_ \\ / _` | '_ \\ / _` | '__/ _` |\n");
    printf(VERMELHO"                                           / ____ \\  | (_| | \\__ \\ |_) | |_| | || (_| | | |_) |  __/ | (_| | | |__| | |_| | (_| | | | | (_| | |_) | (_| | | | (_| |\n");
    printf(VERMELHO"                                          /_/    \\_\\  \\__,_|_|___/ .__/ \\__,_|\\__\\__,_| | .__/ \\___|_|\\__,_|  \\_____|\\__,_|\\__,_|_| |_|\\__,_|_.__/ \\__,_|_|  \\__,_|\n");
    printf(VERMELHO"                                                                 | |                    | |\n");
    printf(VERMELHO"                                                                 |_|                    |_|\n"COR_RESET);

    printf(VERDE"                                         ______  _       _                _______                                                      _       _       _             \n");
    printf("                                        (______)(_)     (_)  _           (_______)       _                                            (_)     (_)     (_)            \n");
    printf("                                         _     _ _  ____ _ _| |_ _____    _____   ____ _| |_ _____  ____    ____  _____  ____ _____    _ ____  _  ____ _ _____  ____ \n");
    printf("                                        | |   | | |/ _  | (_   _) ___ |  |  ___) |  _ (_   _) ___ |/ ___)  |  _ \\(____ |/ ___|____ |  | |  _ \\| |/ ___) (____ |/ ___)\n");
    printf("                                        | |__/ /| ( (_| | | | |_| ____|  | |_____| | | || |_| ____| |      | |_| / ___ | |   / ___ |  | | | | | ( (___| / ___ | |    \n");
    printf("                                        |_____/ |_|\\___ |_|  \\__)_____)  |_______)_| |_| \\__)_____)_|      |  __/\\_____|_|   \\_____|  |_|_| |_|_|\\____)_\\_____|_|    \n");
    printf("                                                  (_____|                                                  |_|                                                       \n"COR_RESET);

    system("read -p 'Pressione enter para continuar...' var"); /* Pausa o programa até que o usuário esteja pronto */
    printf("\a");

    printf("\e[2J\e[H"); /* Limpa a tela do terminal */

    return(0);
}

char * valida_string(int parametro) { /* Função para validar strings (os parâmetros são para diferenciar se está sendo pedido o nome ou sexo do player */

    int controle_str; /* variável de controle 1 */
    int controle_tam_str; /* segunda variável de controle */
    int j; /* variável para percorrer a string que está sendo validada */
    char * string_ = (char *) malloc(21); /* variável local que recebe a string que será validada */

    char sexo_f[] = "feminino";

    char sexo_m[] = "masculino";

    while(1) { /* loop infinito até o usuário digitar corretamente */

        controle_str = 1;

        controle_tam_str = 1;

        gets(string_);

        if(strlen(string_) == 0) { /* testa se o usuário não digitou nada */

            controle_tam_str = 0; /* se o usuário não digitou nada, registra tal fato na variável */

        }

        if(controle_tam_str) { /* se passar do primeiro teste, testa para ver se o usuário não digitou nenhum número na string */

            for(j = 0; j < strlen(string_); j++) {

                if(isdigit(string_[j])) {

                    controle_str = 0;
                    printf("Você digitou um número, o que é inválido. Tente novamente.\n");
                    break;

                }

            }

        }

        else { /* informa, caso o usuário não tenha digitado nada, para ele tentar novamente */

            printf("Você não digitou corretamente. Tente novamente.\n");
            controle_str = 0;

        }

        if((controle_str) && (parametro == 1)) {

            break;

        }

        else if((controle_str) && (parametro == 2)) {

            if((strcmp(string_, sexo_f) == 0) || (strcmp(string_, sexo_m) == 0)) {

                break;

            }

        }

    }

    return(string_); /* retorna a string devidamente validada */

}

void introducao(void) {

    printf(VERDE" ______  _       _                                                                 \n");
    printf("(______)(_)     (_)  _                                                             \n");
    printf(" _     _ _  ____ _ _| |_ _____     ___ _____ _   _    ____   ___  ____  _____    _ \n");
    printf("| |   | | |/ _  | (_   _) ___ |   /___) ___ | | | |  |  _ \\ / _ \\|    \\| ___ |  (_)\n");
    printf("| |__/ /| ( (_| | | | |_| ____|  |___ | ____| |_| |  | | | | |_| | | | | ____|   _ \n");
    printf("|_____/ |_|\\___ |_|  \\__)_____)  (___/|_____)____/   |_| |_|\\___/|_|_|_|_____)  (_)\n");
    printf("          (_____|                                                                  \n\n"COR_RESET);

    PLAYER.nome = valida_string(1); /* Programa pede o nome do usuário */

    printf("\e[2J\e[H"); /* Limpa a tela */

    printf(VERDE" ______  _       _                                                          \n");
    printf("(______)|_|     (_)  _                                                      \n");
    printf(" _     _ _  ____ _ _| |_ _____     ___ _____ _   _     ___ _____ _   _ ___  \n");
    printf("| |   | | |/ _  | (_   _) ___ |   /___) ___ | | | |   /___) ___ ( \\ / ) _ \\ \n");
    printf("| |__/ /| ( (_| | | | |_| ____|  |___ | ____| |_| |  |___ | ____|) X ( |_| |\n");
    printf("|_____/ |_|\\___ |_|  \\__)_____)  (___/|_____)____/   (___/|_____|_/ \\_)___/ \n");
    printf("          (_____|                                                           \n"COR_RESET);

    /* falta validar */
    printf("(masculino ou feminino): ");

        PLAYER.sexo = valida_string(2); /* Programa pede o sexo do usuário */

    /* ---------------------------------------------------------------------------------------------------------------------------------------------------------------- */

    printf("\e[2J\e[H"); /* Limpa a tela */

    /* Será printada uma pequena historinha para criar um contexto ao jogo */

    printf(VERDE"                                 .______   .______       ___________    ____  _______        __    __   __       _______.___________.  ______   .______       __    ______   ______   \n");
    printf("                                 |   _  \\  |   _  \\     |   ____\\   \\  /   / |   ____|      |  |  |  | |  |     /       |           | /  __  \\  |   _  \\     |  |  /      | /  __  \\  \n");
    printf("                                 |  |_)  | |  |_)  |    |  |__   \\   \\/   /  |  |__         |  |__|  | |  |    |   (----`---|  |----`|  |  |  | |  |_)  |    |  | |  ,----'|  |  |  | \n");
    printf("                                 |   _  <  |      /     |   __|   \\      /   |   __|        |   __   | |  |     \\   \\       |  |     |  |  |  | |      /     |  | |  |     |  |  |  | \n");
    printf("                                 |  |_)  | |  |\\  \\----.|  |____   \\    /    |  |____       |  |  |  | |  | .----)   |      |  |     |  `--'  | |  |\\  \\----.|  | |  `----.|  `--'  | \n");
    printf("                                 |______/  | _| `._____||_______|   \\__/     |_______|      |__|  |__| |__| |_______/       |__|      \\______/  | _| `._____||__|  \\______| \\______/  \n\n\n"COR_RESET);

    char frase1[] = {"--> Em 1889, no Brasil, ocorre um Golpe de Estado.\n"};
    char frase2[] = {"--> A recém-criada República, contudo, continua com as bases escravistas do antigo Regime...\n"};
    char frase3[] = {"--> A exemplo disso, há o uso da chibata como forma de punição à marinheiros negros.\n"};
    char frase4[] = {"--> Nesse contexto, ocorre, em 1910, uma revolta da Marinha Brasileira na Baía de Guanabara, no Rio.\n"};

    printf("%s", frase1);
    usleep(1978000);
    printf("%s", frase2);
    usleep(1978000);
    printf("%s", frase3);
    usleep(1978000);
    printf("%s", frase4);
    usleep(1978000);

    system("read -p 'Pressione enter para continuar...' var"); /* Pausa o programa até que o usuário esteja pronto */
    fflush(stdin);
    printf("\e[2J\e[H"); /* Limpa a tela */


    printf(VERDE"                                 .______   .______       ___________    ____  _______        __    __   __       _______.___________.  ______   .______       __    ______   ______   \n");
    printf("                                 |   _  \\  |   _  \\     |   ____\\   \\  /   / |   ____|      |  |  |  | |  |     /       |           | /  __  \\  |   _  \\     |  |  /      | /  __  \\  \n");
    printf("                                 |  |_)  | |  |_)  |    |  |__   \\   \\/   /  |  |__         |  |__|  | |  |    |   (----`---|  |----`|  |  |  | |  |_)  |    |  | |  ,----'|  |  |  | \n");
    printf("                                 |   _  <  |      /     |   __|   \\      /   |   __|        |   __   | |  |     \\   \\       |  |     |  |  |  | |      /     |  | |  |     |  |  |  | \n");
    printf("                                 |  |_)  | |  |\\  \\----.|  |____   \\    /    |  |____       |  |  |  | |  | .----)   |      |  |     |  `--'  | |  |\\  \\----.|  | |  `----.|  `--'  | \n");
    printf("                                 |______/  | _| `._____||_______|   \\__/     |_______|      |__|  |__| |__| |_______/       |__|      \\______/  | _| `._____||__|  \\______| \\______/  \n\n\n"COR_RESET);


    printf("Sabe-se que a revolta foi, apesar de conseguir abolir o uso da chibata na marinha, reprimida...\n");
    usleep(1978000);
    printf("Mas e se você pudesse voltar no tempo e mudar o rumo da história?\n");
    usleep(1978000);

    system("read -p 'Pressione enter para continuar...' var"); /* Pausa o programa até que o usuário esteja pronto */

    printf("\e[2J\e[H"); /* Limpa a tela */

    printf(VERDE"___________       __                                                             __         .__               \n");
    printf("\\_   _____/ _____/  |________   ____       ____   ____     ______   ____________/  |______  |  |              \n");
    printf(" |    __)_ /    \\   __\\_  __ \\_/ __ \\     /    \\ /  _ \\    \\____ \\ /  _ \\_  __ \\   __\\__  \\ |  |              \n");
    printf(" |        \\   |  \\  |  |  | \\/\\  ___/    |   |  (  <_> )   |  |_> >  <_> )  | \\/|  |  / __ \\|  |__            \n");
    printf("/_______  /___|  /__|  |__|    \\___  >   |___|  /\\____/    |   __/ \\____/|__|   |__| (____  /____/ /\\  /\\  /\\ \n");
    printf("        \\/     \\/                  \\/         \\/           |__|                           \\/       \\/  \\/  \\/ \n"COR_RESET);

    usleep(100000);
    printf(MAGENTA"             .,-:;//;:=,\n");
    usleep(100000);
    printf("         . :H@@@MM@M#H/.,+%%;,\n");
    usleep(100000);
    printf("      ,/X+ +M@@M@MM%%=,-%%HMMM@X/,\n");
    usleep(100000);
    printf("     -+@MM; $M@@MH+-,;XMMMM@MMMM@+-\n");
    usleep(100000);
    printf("    ;@M@@M- XM@X;. -+XXXXXHHH@M@M#@/.\n");
    usleep(100000);
    printf("  ,%%MM@@MH ,@%%=            .---=-=:=,.\n");
    usleep(100000);
    printf("  -@#@@@MX .,              -%%HX$$%%%%+;\n");
    usleep(100000);
    printf(" =-./@M@M$                  .;@MMMM@MM:\n");
    usleep(100000);
    printf(" X@/ -$MM/                    .+MM@@@M$\n");
    usleep(100000);
    printf(",@M@H: :@:                    . -X#@@@@-\n");
    usleep(100000);
    printf(",@@@MMX, .                    /H- ;@M@M=\n");
    usleep(100000);
    printf(".H@@@@M@+,                    %%MM+..%%#$.\n");
    usleep(100000);
    printf(" /MMMM@MMH/.                  XM@MH; -;\n");
    usleep(100000);
    printf("  /%%+%%$XHH@$=              , .H@@@@MX,\n");
    usleep(100000);
    printf("   .=--------.           -%%H.,@@@@@MX,\n");
    usleep(100000);
    printf("   .%%MM@@@HHHXX$$$%%+- .:$MMX -M@@MM%%.\n");
    usleep(100000);
    printf("     =XMMM@MM@MM#H;,-+HMM@M+ /MMMX=\n");
    usleep(100000);
    printf("       =%%@M@M#@$-.=$@MM@@@M; %%M%%=\n");
    usleep(100000);
    printf("         ,:+$+-,/H#MMMMMMM@- -,\n");
    usleep(100000);
    printf("               =++%%%%%%%%+/:-.\n"COR_RESET);
    usleep(100000);

    system("read -p 'Pressione enter para entrar no portal...' var"); /* Pausa o programa até que o usuário esteja pronto */

    printf("\e[2J\e[H"); /* Limpa a tela */

    printf(VERDE"                                                                                                                       ___   _____  ___   _____ \n");
    printf("                                                                                                                     /   | /  _  \\/   | /  _  \\\n");
    printf("                                                                                                                      |  | \\___  | |  | |  |  |\n");
    printf("                                                                                                                     <____>|_____/<____>\\_____/\n"COR_RESET);

    printf("--> É 22 de novembro.\n");
    usleep(1978000);

    printf("--> Você, %s, faz parte do motim naval no Rio.\n", PLAYER.nome);
    usleep(1978000);

    char feminina[] = "feminino";
    /* Checa-se o sexo do jogador para empregar o artigo correto na hora de se referir ao jogador(a) */
    if(strcmp(PLAYER.sexo, feminina) == 0) {

        printf("--> Os marinheiros lhe escolheram para ser a líder da revolta e comandar a esquadra naval em uma possível investida contra a República.\n");

    }

    else {

        printf("--> Os marinheiros lhe escolheram para ser o líder da revolta e comandar a esquadra naval em uma possível investida contra a República.\n");

    }

    usleep(1978000);
    printf("--> Você decide iniciar a tomada da Baía de Guanabara...\n");
    usleep(1978000);
    printf("Conseguirás mudar o rumo da história?\n");
    usleep(1978000);
    system("read -p 'Pressione enter para continuar...' var"); /* Pausa o programa até que o usuário esteja pronto */

    /* Printa um navio de guerra antes do jogo começar de fato (estética Front-End) */
    int vezes;
    int auxilia;
    for(vezes = 0; vezes < 65; vezes++) {
        for(auxilia = 0; auxilia < vezes; auxilia++) {

            printf(" ");

        }
        printf(VERDE"                                     # #  ( )\n"COR_RESET);
        for(auxilia = 0; auxilia < vezes; auxilia++) {

            printf(" ");

        }
        printf(VERDE"                                  ___#_#___|__\n"COR_RESET);
        for(auxilia = 0; auxilia < vezes; auxilia++) {

            printf(" ");

        }
        printf(VERDE"                              _  |____________|  _\n"COR_RESET);
        for(auxilia = 0; auxilia < vezes; auxilia++) {

            printf(" ");

        }
        printf(VERDE"                       _=====| | |            | | |==== _\n"COR_RESET);
        for(auxilia = 0; auxilia < vezes; auxilia++) {

            printf(" ");

        }
        printf(VERDE"                 =====| |.---------------------------. | |====\n"COR_RESET);
        for(auxilia = 0; auxilia < vezes; auxilia++) {

            printf(" ");

        }
        printf(VERDE"   <--------------------'   .  .  .  .  .  .  .  .   '--------------/\n"COR_RESET);
        for(auxilia = 0; auxilia < vezes; auxilia++) {

            printf(" ");

        }
        printf(VERDE"     \\                                                             /\n"COR_RESET);
        for(auxilia = 0; auxilia < vezes; auxilia++) {

            printf(" ");

        }
        printf(VERDE"      \\_______________________________________________WWS_________/\n"COR_RESET);
        for(auxilia = 0; auxilia < vezes; auxilia++) {

            printf(" ");

        }
        printf(VERDE"wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww\n"COR_RESET);
        for(auxilia = 0; auxilia < vezes; auxilia++) {

            printf(" ");

        }
        printf(VERDE"wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww\n"COR_RESET);
        for(auxilia = 0; auxilia < vezes; auxilia++) {

            printf(" ");

        }
        printf(VERDE"   wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww \n"COR_RESET);

        usleep(100000); /* Retarda o programa com fins estéticos */
        printf("\e[2J\e[H"); /* Limpa a tela */

    }



}
/*========================================================
          FUNÇÕES DE MONTAGEM DE TABULEIRO              */

void gera_tabuleiro(short int * ponteiro_para_tabuleiro) {
    int i;
    /*For passAndo por todos os pontos do tabuleiro (TAM*TAM é a área)
    mudando o valor do tabuleiro para 0 "mar" que depois poderá ser alterado
    e em seguida direcionando o ponteiro para a próxima coordenada do tabuleiro*/
    for(i = 0; i < TAM * TAM; i++){
        *ponteiro_para_tabuleiro = 0;
        ponteiro_para_tabuleiro++;
    }
}

int valida_criacao(short int * ponteiro_para_tabuleiro, int alvo){
    /*Valida se a coordenada já tem o pedaço ou algumcação, caso tenha retorna
    erro com 2*/
    ponteiro_para_tabuleiro += alvo; /*aponta ponteiro para alvo*/
    if(*ponteiro_para_tabuleiro == 1){
        printf("Ops, parece que já temos uma embarcação ai, escolha outro lugar\n");
        return 2;/*Erro*/
    }
    else return 0;
}

int verifica_embarcacao(short int primeira_coordenada,short int ultima_coordenada,short int tamanho_da_embarcacao){
    /*utiliza esquema de quociente e resto, para com quociente ser a linha
    e o resto para ser a coluna. Assim verifica se a embarcação tem o tamanho
    correto em relação ao esperado.*/
    int quociente_primeira, quociente_ultima;
    int resto_primeira, resto_ultima;

    /*Transforma os valores para as variaveis quociente e resto*/
    quociente_primeira = primeira_coordenada/TAM;
    resto_primeira = primeira_coordenada%TAM;
    quociente_ultima = ultima_coordenada/TAM;
    resto_ultima = ultima_coordenada%TAM;

    /*Caso estejam na mesma coluna*/
    if(resto_ultima == resto_primeira){
        /*valida tamanho da embarcação solicitada, fazendo correções no valor*/
        if(quociente_ultima - quociente_primeira + 1 == tamanho_da_embarcacao ||  quociente_primeira - quociente_ultima + 1   == tamanho_da_embarcacao){
            return 1;
        }
        else{
            printf("A embarcação mede %d, mude as coordenadas por favor\n",tamanho_da_embarcacao);
            return 0;
        }
    }
        /*Caso estejam na mesma linha*/
    else if(quociente_ultima == quociente_primeira){
        /*valida tamanho da embarcação solicitada, fazendo correções no valor*/
        if(resto_ultima - resto_primeira + 1 == tamanho_da_embarcacao || resto_primeira - resto_ultima + 1 == tamanho_da_embarcacao){
            return 1;
        }
        else{
            printf("A embarcação mede %d, mude as coordenadas por favor\n",tamanho_da_embarcacao);
            return 0;
        }
    }
    else{/*se chegar aqui significa que não possue nem linha nem coluna em comum, logo
    se retorna a função anterior para nova inserção de coordenadas*/
        printf("As embarcações não podem ficar na diagonal\n");
        return 0;
    }
}

int cria_embarcacao(short int * ponteiro_para_tabuleiro,int primeira_coordenada,int ultima_coordenada,int tamanho_da_embarcacao){
    /*Função cria embarcação se tudo der certo, retorna 2 em caso de erro*/
    int temporario, i, alvo;
    /*coloca o menor valor na primeira coordenada*/
    if(primeira_coordenada > ultima_coordenada){
        temporario = ultima_coordenada;
        ultima_coordenada = primeira_coordenada;
        primeira_coordenada = temporario;
    }
    /*coloca o alvo na primeira_coordenada*/
    alvo = primeira_coordenada;
    /*Caso a embarcação esteja na vertical*/
    if((ultima_coordenada - primeira_coordenada) >= TAM){
        for(i = 0; i < tamanho_da_embarcacao; i++){
            /*faz a validação se o alvo solicitado não está sobre alguma embarcação já alocada*/
            if(valida_criacao(ponteiro_para_tabuleiro, alvo) == 0){
                alvo +=TAM;/*manda alvo para próximo ponto da embarcação*/
            }
            else return 2;/*Erro*/
        }
    }
        /*Caso a embaracação esteja na horizontal*/
    else{
        for(i = 0; i < tamanho_da_embarcacao; i++){
            if(valida_criacao(ponteiro_para_tabuleiro, alvo) == 0){
                alvo++;
            }
            else return 2;/*Erro*/
        }
    }
    alvo = primeira_coordenada; /*Corrige posição de alvo*/
    for(i = 0; i < tamanho_da_embarcacao; i++){
        muda_valor(ponteiro_para_tabuleiro, alvo); /*muda valor do tabuleiro onde está o alvo*/
        /*Caso a embarcação esteja na vertical*/
        if((ultima_coordenada - primeira_coordenada) >= TAM){
            alvo += TAM;
        }
            /*caso esteja na horizontal*/
        else alvo++;
    }
    return 0;
}

void coloca_embarcacoes(short int * ponteiro_para_tabuleiro,int tamanho_da_embarcacao, int quantidade_de_embarcacoes){
    /*função que coloca varias embarcações no tabuleiro dependendo do tamanho_da_entrada
    fazendo anteriormente as validações*/
    int i,primeira_coordenada,ultima_coordenada;/*coordenada da primeira ponta da embarcação e da ultima*/
    int verifica = 0,verifica2 = 2;
    /*looping por dentro do total de embarcações do tipo*/
    for(i = 0; i < quantidade_de_embarcacoes; i++){
        printf("(Ainda faltam %d embarcações desse tipo, elas medem %d)\n", quantidade_de_embarcacoes - i,tamanho_da_embarcacao);
        while(verifica2 != 0){
            while(verifica != 1){
                /*Recebe coordenadas*/
                printf("Vamos a coordenada da primeira ponta da embarcação\n");
                primeira_coordenada = recebe_coordenadas();
                printf("Agora a coordenada da ultima ponta da embarcação\n");
                ultima_coordenada = recebe_coordenadas();
                /*faz a validação da embarcação*/
                verifica += verifica_embarcacao(primeira_coordenada,ultima_coordenada,tamanho_da_embarcacao);
            }verifica = 0;/*Corrige Verifica para 0*/
            if(cria_embarcacao(ponteiro_para_tabuleiro,primeira_coordenada,ultima_coordenada,tamanho_da_embarcacao) == 2){
                /*Avalia se algum ponto da embarcação está sobre ontro já incluído, caso sim mantém verifica2 em 2 */
                verifica2 = 2;
            }
            else verifica2 = 0; /*sai do loop, caso embarcação seja alocada corretamente*/
        }verifica2 = 2;
        imprime_tabuleiro(ponteiro_para_tabuleiro);
    }
}

int cria_submarino(short int * ponteiro_para_tabuleiro,int coordenada){
    /*Função cria embarcação se tudo der certo, retorna 2 em caso de erro*/
    if(valida_criacao(ponteiro_para_tabuleiro, coordenada) == 0){
        muda_valor(ponteiro_para_tabuleiro, coordenada);/*muda valor no tabuleiro onde está o alvo*/
    }
    else return 2;/*Erro*/
    return 0;
}

void coloca_submarinos(short int * ponteiro_para_tabuleiro){
    /*função que coloca varios submarinos no tabuleiro fazendo anteriormente as validações*/
    int i,coordenada;/*coordenada da embarcação*/
    int verifica = 2;
    /*looping por dentro do total de submarinos*/
    for(i = 0; i < QNTSUB; i++){
        printf("Ainda faltam %d embarcações desse tipo\n", QNTSUB - i);
        while(verifica == 2){
            printf("Diga a coordenada da embarcação\n");
            coordenada = recebe_coordenadas();
            if(cria_submarino(ponteiro_para_tabuleiro,coordenada) == 2){
                /*Avalia se algum ponto da embarcação está sobre ontro já incluído, caso sim
                mantem verifica = 2 a entrada*/
                verifica = 2;
            }
            else verifica = 0;
        }verifica = 2;/*Corrige Verifica para 2*/
        imprime_tabuleiro(ponteiro_para_tabuleiro);
    }
}

void monta_tabuleiro_jogador(short int * ponteiro_para_tabuleiro){
    /*DIALOGO*/
    int i;
    printf("[Capitão-Tenente] :\n");
    printf("Que bom que chegou, comandante %s, estava ao seu aguardo.\n",PLAYER.nome);
    printf("Não temos tempo, a esquadra inimiga se aproxima");
    for(i=0;i < 3; i++){ /* abrir um laço de repetição com for */
        system("sleep 01"); /* pausa de 1 segundo */
        printf("."); /* escrever 1 "." na tela */
        fflush(stdout); /* atualizar a tela */
    }
    printf("\n");
    printf("Preciso que ordene como vamos colocar nossa esquadra.\n");
    printf("Pressione ENTER:\n");
    getchar();
    printf("\e[2J\e[H");
    /*PORTA AVIÕES*/
    printf("\n Vou fazer o telefonema para o capitão, um momento\n");
    for(i=0;i < 3; i++){ /* abrir um laço de repetição com for */
        system("sleep 01"); /* pausa de 1 segundo */
        printf("."); /* escrever 1 "." na tela */
        fflush(stdout); /* atualizar a tela */
    }
    printf("\n");
    printf("Ele atendeu. Começamos pelo porta aviões. Aguardamos suas ordens \n");
    imprime_tabuleiro(ponteiro_para_tabuleiro);

    coloca_embarcacoes(ponteiro_para_tabuleiro,TAMPA,QNTPA);
    /*ENCOURAÇADOS*/
    printf("\e[2J\e[H");
    imprime_tabuleiro(ponteiro_para_tabuleiro);
    printf("Agora temos que colocar seus encouraçados.\n");
    coloca_embarcacoes(ponteiro_para_tabuleiro,TAMEN, QNTEN);
    /*Cruzadores*/
    printf("\e[2J\e[H");
    imprime_tabuleiro(ponteiro_para_tabuleiro);
    printf("Agora é a vez dos cruzadores.\n");
    coloca_embarcacoes(ponteiro_para_tabuleiro,TAMCR, QNTCR);
    /*SUBMARINOS*/
    printf("\e[2J\e[H");
    imprime_tabuleiro(ponteiro_para_tabuleiro);
    printf("Por último os submarinos.\n");
    coloca_submarinos(ponteiro_para_tabuleiro);
}

int coloca_embarcacoes_versao_auto(short int * ponteiro_para_tabuleiro,int tamanho_da_embarcacao,int quantidade_de_embarcacoes){
    int i,vertical_ou_horizontal,quociente,resto;
    int primeira_coordenada,ultima_coordenada;
    srand(time(NULL));
    for(i = 0; i < quantidade_de_embarcacoes; i++){
        while(1) {
            vertical_ou_horizontal = rand()%2;
            /*define 0 para posição vertical da embarcação, 1 para horizontal*/
            if(vertical_ou_horizontal == 0){
                quociente = rand()%(TAM - tamanho_da_embarcacao + 1);/*sorteia uma linha dentre as possíveis para a primeira*/
                resto = rand()%8;/*sorteia a coluna*/
                primeira_coordenada = quociente * TAM + resto;
                ultima_coordenada = primeira_coordenada + (TAM * tamanho_da_embarcacao);
            }
            else{
                quociente = rand()%8;/*sorteia uma linha*/
                resto = rand()%(TAM - tamanho_da_embarcacao + 1);/*sorteia a coluna dentre as possíveis para a primeira*/
                primeira_coordenada = quociente * TAM + resto;
                ultima_coordenada = quociente * TAM + tamanho_da_embarcacao - 1 + resto;
            }
            /*valida criação da embarcação, caso dê erro recebe da função cria_embarcacao e continua no while
            caso bem sucedido dá break, e embarcação é criada dentro da função chamada*/
            if(cria_embarcacao(ponteiro_para_tabuleiro,primeira_coordenada,ultima_coordenada,tamanho_da_embarcacao) == 2){
                continue;
            }
            else break;
        }
    }
    return 0;
}

void coloca_submarinos_versao_auto(short int * ponteiro_para_tabuleiro){
    /*função que coloca varios submarinos no tabuleiro fazendo anteriormente as validações*/
    int i,coordenada;/*coordenada da embarcação*/
    int verifica = 2;
    srand(time(NULL));
    /*looping por dentro do total de submarinos*/
    for(i = 0; i < QNTSUB; i++){
        while(verifica == 2){
            coordenada = rand()%64 ; /*cria coordenada aleatória*/
            if(cria_submarino(ponteiro_para_tabuleiro,coordenada) == 2){
                /*Avalia se algum ponto da embarcação está sobre ontro já incluído, caso sim
                mantem verifica = 2 a entrada*/
                verifica = 2;
            }
            else verifica = 0;
        }verifica = 2;/*Corrige Verifica para 2*/
    }
}

void monta_tabuleiro_auto(short int * ponteiro_para_tabuleiro){

    /*PORTA AVIÕES*/
    coloca_embarcacoes_versao_auto(ponteiro_para_tabuleiro,TAMPA,QNTPA);
    /*ENCOURAÇADOS*/
    coloca_embarcacoes_versao_auto(ponteiro_para_tabuleiro,TAMEN, QNTEN);
    /*CRUZADORES*/
    coloca_embarcacoes_versao_auto(ponteiro_para_tabuleiro,TAMCR, QNTCR);
    /*SUBMARINOS*/
    coloca_submarinos_versao_auto(ponteiro_para_tabuleiro);
}
/*========================================================
                FUNÇÕES DE MEIO DE JOGO                  */

int tiro_jogador(short int * ponteiro_para_tabuleiro){
    int i,entrada,valor = 2;
    while(valor == 2){/*Não sai do loop enquanto atirar em lugar previamente atirado*/
        printf("Diga a coordenada do ataque comandante %s!\n", PLAYER.nome);
        entrada = recebe_coordenadas();/*recebe a posição da coordenada no vetor pela função*/
        if(retorna_valor_tabuleiro(ponteiro_para_tabuleiro,entrada) == 0){/*Ponteiro estara apontado paprintf("ponteiro  \n", ponteiro_para_tabuleiro);printf("ponteiro  \n", ponteiro_para_tabuleiro);ra coordenada contendo água*/
            printf("Tiro na Água...\n");
            for(i = 0; i < 3; i++) muda_valor(ponteiro_para_tabuleiro,entrada);/*corrige Mar para "Tiro na Água"*/
            return 0;/*Retorna 0 para tiro na água*/
        }
        else if(retorna_valor_tabuleiro(ponteiro_para_tabuleiro,entrada) == 1){/*Ponteiro estara apontado para coordenada contendo embarcação não atingida*/
            printf("Acertamos uma embarcação!!!\n");
            muda_valor(ponteiro_para_tabuleiro, entrada);/*muda de embarcação não atingida para atingida*/
            return 1;/*retorna 1 para ataque bem sucedido*/
        }
        else{/*Ponteiro estará apontado para embarcação contendo coordenada já atingida*/
            printf("Favor atirar em outro lugar, Já disparamos nesse local\n");
        }
    }
}

int tiro_computador(short int * ponteiro_para_tabuleiro,short int * ultimo_ataque,short int coordenada){
    int i;
    if(coordenada >= 0 || coordenada <= 63){
      if(retorna_valor_tabuleiro(ponteiro_para_tabuleiro,coordenada)== 0){
        printf("Eles atiraram na Água\n");
        for(i = 0; i < 3; i++) muda_valor(ponteiro_para_tabuleiro,coordenada);/*corrige Mar para "Tiro na Água"*/
        *ultimo_ataque = coordenada;/*registra o ataque como ultimo*/
        return 0;/*retorna 0 para tiro na água*/
      }
      else if(retorna_valor_tabuleiro(ponteiro_para_tabuleiro,coordenada ) == 1){/*Ponteiro estara apontado para coordenada contendo embarcação não atingida*/
        printf("Oh não! Acertaram uma das nossas embarcações\n");
        muda_valor(ponteiro_para_tabuleiro, coordenada);/*muda de embarcação não atingida para atingida*/
        *ultimo_ataque = coordenada;/*registra o ataque como ultimo*/
        return 1; /*retorna 1 para embarcação atingida*/
      }
      else{/*Ponteiro estará apontado para embarcação contendo coordenada já atingida*/
        return 2; /*Retorna 2 para que o ataque seja refeito*/
      }
    }
    else{
      return 2;
    }
}

int ataque_computador(short int * ponteiro_para_tabuleiro,short int * ultimo_ataque,short int * sucesso_ultimo_ataque,short int * sentido_direcao){
    /*Fução recebe ponteiro para tabuleiro, o ponteiro apontando para coordennóada do ulnótimo ataque, se o ultimo ataque foi bem sucedido ou não
    e se o ataque já tem uma orientação para sentido e direção
    sentido_direcao vai definir para onde prosseguir após um ataque bem sucedido:
    Caso 0 => Aleatório
    Caso 1 => Para cima
    Caso 2 => Para a esquerda
    Caso 3 => Para baixo
    Caso 4 => Para direita  */
    int sucesso_ataque = 2;/*Recebe se ataque foi bem sucedido, mal sucedido ou se deve ser repetido*/
    int dado; /*Sorteia sentido para o ataque*/
    int temos_uma_nova_direcao,/*valor a ser armazenado por sentido_direcao*/coordenada;
    srand(time(NULL));
    if(*sucesso_ultimo_ataque == 1 && *sentido_direcao > 0){
        if(*sentido_direcao % 2 == 1){ /*Caso seja impar ou seja para cima(1) ou pra baixo(3)*/
            coordenada = *ultimo_ataque + (TAM * (2 - *sentido_direcao));
            sucesso_ataque = tiro_computador(ponteiro_para_tabuleiro,ultimo_ataque,coordenada);
            if(sucesso_ataque == 2){/*Em caso de erro no tiro (tiro já efetuado), vai na direção oposta*/
                coordenada = *ultimo_ataque - (TAM * (2 - *sentido_direcao));
                sucesso_ataque = tiro_computador(ponteiro_para_tabuleiro,ultimo_ataque,coordenada);
                if(sucesso_ataque == 2){/*Em caso de erro no tiro (tiro já efetuado), tenta tiro aleatório*/
                    while(sucesso_ataque == 2){
                        coordenada = rand()%64; /*Sorteia valor de 0 a 63*/
                        sucesso_ataque = tiro_computador(ponteiro_para_tabuleiro,ultimo_ataque,coordenada);
                    }
                    *sucesso_ultimo_ataque = sucesso_ataque;/*Atualiza quanto ao sucesso*/
                    *sentido_direcao = 0;/*Atualiza sentido e direção para 0*/
                    return 0;/*retorna sem erros*/
                }
                else if(sucesso_ataque == 1){
                    *sentido_direcao = (*sentido_direcao + 2)%4;/*Atualiza sentido e direção para 0para o outro lado e mantem a direção*/
                    return 0;
                }
                else{
                    *sentido_direcao = 0;/*Atualiza sentido e direção para 0*/
                    return 0;
                }
            }
            else if(sucesso_ataque == 1){
                *sentido_direcao = (*sentido_direcao + 2)%4;/*Atualiza sentido e direção para 0para o outro lado e mantem a direção*/
                return 0;
            }
            else {
                *sentido_direcao = 0;/*Atualiza sentido e direção para 0*/
                return 0;
            }
            *sucesso_ultimo_ataque = sucesso_ataque;
            return 0;
        }
        if(*sentido_direcao % 2 == 0){ /*Caso seja par ou seja para esquerda(2) ou pra direita(0)*/
            coordenada = *ultimo_ataque + (TAM * (2 - *sentido_direcao));
            sucesso_ataque = tiro_computador(ponteiro_para_tabuleiro,ultimo_ataque,coordenada);
            if(sucesso_ataque == 2){/*Em caso de erro no tiro (tiro já efetuado), vai na direção oposta*/
                coordenada = *ultimo_ataque - (TAM * (2 - *sentido_direcao));
                sucesso_ataque = tiro_computador(ponteiro_para_tabuleiro,ultimo_ataque,coordenada);
                if(sucesso_ataque == 2){/*Em caso de erro no tiro (tiro já efetuado), tenta tiro aleatório*/
                    while(sucesso_ataque == 2){
                        coordenada = rand()%64; /*Sorteia valor de 0 a 63*/
                        sucesso_ataque = tiro_computador(ponteiro_para_tabuleiro,ultimo_ataque,coordenada);
                    }
                    *sucesso_ultimo_ataque = sucesso_ataque;/*Atualiza quanto ao sucesso*/
                    *sentido_direcao = 0;/*Atualiza sentido e direção para 0*/
                    return 0;/*retorna sem erros*/
                }
                else if(sucesso_ataque == 1){
                    *sentido_direcao = (*sentido_direcao + 2)%4;/*Atualiza sentido e direção para 0para o outro lado e mantem a direção*/
                    return 0;
                }
                else{
                    *sentido_direcao = 0;/*Atualiza sentido e direção para 0*/
                    return 0;
                }
            }
            else if(sucesso_ataque == 1){
                *sentido_direcao = (*sentido_direcao + 2)%4;/*Atualiza sentido e direção para para o outro lado e mantem a direção*/
                return 0;
            }
            else {
                *sentido_direcao = 0;/*Atualiza sentido e direção para 0*/
                return 0;
            }
            *sucesso_ultimo_ataque = sucesso_ataque;
            return 0;
        }
    }
    else if(*sucesso_ultimo_ataque == 1 && *sentido_direcao == 0){
        /*teve um ataque bem sucedido anteriormente
        porém não sabe o sentido e direção da embarcação*/
        dado = rand()%4;
        switch (dado) {
            /*começa tentando buscando um tiro na água ou acertar um navio, caso encontre algum tiro já efetuado
            ou navio afundado chuta outra posição qualquer*/
            case 1:
                coordenada = *ultimo_ataque - TAM;
                sucesso_ataque = tiro_computador(ponteiro_para_tabuleiro,ultimo_ataque,coordenada);
                if(sucesso_ataque != 2){
                    *sucesso_ultimo_ataque = sucesso_ataque;
                    temos_uma_nova_direcao = 1;
                    break;
                }
            case 2:
                coordenada = *ultimo_ataque - 1;
                sucesso_ataque = tiro_computador(ponteiro_para_tabuleiro,ultimo_ataque,coordenada);
                if(sucesso_ataque != 2){
                    *sucesso_ultimo_ataque = sucesso_ataque;
                    temos_uma_nova_direcao = 2;
                    break;
                }
            case 3:
                coordenada = *ultimo_ataque + TAM;
                sucesso_ataque = tiro_computador(ponteiro_para_tabuleiro,ultimo_ataque,coordenada);
                if(sucesso_ataque != 2){
                    *sucesso_ultimo_ataque = sucesso_ataque;
                    temos_uma_nova_direcao = 3;
                    break;
                }
            case 4:
                coordenada = *ultimo_ataque + 1;
                sucesso_ataque = tiro_computador(ponteiro_para_tabuleiro,ultimo_ataque,coordenada);
                if(sucesso_ataque != 2){
                    *sucesso_ultimo_ataque = sucesso_ataque;
                    temos_uma_nova_direcao = 4;
                    break;
                }
            default:
                while(sucesso_ataque == 2){
                    coordenada = rand()%64; /*Sorteia valor de 0 a 63*/
                    sucesso_ataque = tiro_computador(ponteiro_para_tabuleiro,ultimo_ataque,coordenada);
                }
                *sucesso_ultimo_ataque = sucesso_ataque;/*Atualiza quanto ao sucesso*/
        }
        if(sucesso_ataque == 1){
            *sentido_direcao =  temos_uma_nova_direcao;
        }
    }
    else{
        /*Caso não tenha ataque bem sucedido no ultimo turno atira aleatoriamente e atualiza valor que o
        ponteiro sucesso_ultimo_ataque aponta*/
        while(sucesso_ataque == 2){
            coordenada = rand()%64; /*Sorteia valor de 0 a 63*/
            sucesso_ataque = tiro_computador(ponteiro_para_tabuleiro,ultimo_ataque,coordenada);
        }
        *sucesso_ultimo_ataque = sucesso_ataque;/*Atualiza quanto ao sucesso*/
        *sentido_direcao = 0;
    }
}

/*========================================================
                  FUNÇÕES DE FIM DE JOGO                  */
int testa_fim(short int * ponteiro_para_tabuleiro){
    /*Avalia tabuleiro para saber se o jogo foi vencido ou não*/
    int i;
    for(i = 0; i < TAM * TAM; i++){
        if(*ponteiro_para_tabuleiro == 1){/*se ainda tiverem embarcações não afundadas retorna 0
      para o jogo continuar*/
            return 0;
        }
        ponteiro_para_tabuleiro++;
    }
    return 1;/*Não há nenhuma embarcação não afundada, retorna 1 para o fim do jogo*/
}
