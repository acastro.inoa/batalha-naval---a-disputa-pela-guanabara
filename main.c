#include "BatalhaNaval.h"

int main(int argc, char const *argv[]) {
    short int tabuleiro_jogador[TAM*TAM],tabuleiro_computador[TAM*TAM];/*tabuleiros do jogador e do computador, são vetores com 3 valores possíveis.
  0,1 e 2. A orientação no tabuleiro é: A1,A2,...,A8,B1,...,B8,...H8*/
    short int *quem_joga, *computador;/*ponteiros para os tabuleiros dos jogadores*/
    /*ponteiro do jogador recebe a posição A1 do tabuleiro*/
    short int infos_computador[3],i,valor=2;
    short int *ultimo_ataque_computador,*sucesso_ultimo_ataque, *sentido_direcao;
    char escolha;

    titulo_in(); /* Função que printa o título do jogo */

    introducao(); /* Função que cria um contexto para o usuário interagir no jogo */

    printf("\e[2J\e[H"); /* Limpa a tela do terminal */

    quem_joga = &tabuleiro_jogador[0];
    computador = &tabuleiro_computador[0];
    ultimo_ataque_computador = &infos_computador[0];
    sucesso_ultimo_ataque = &infos_computador[1];
    *ultimo_ataque_computador = *sucesso_ultimo_ataque = 0;
    sentido_direcao = &infos_computador[2];
    *sentido_direcao = 0;

    gera_tabuleiro(quem_joga);
    gera_tabuleiro(computador);
    monta_tabuleiro_jogador(quem_joga);
    monta_tabuleiro_auto(computador);
    imprime_tabuleiro(computador);
    printf("\e[2J\e[H");
    printf("\e[2J\e[H");
    printf("\e[2J\e[H");
    printf("\e[2J\e[H");
    printf("\e[2J\e[H");
    printf("Pressione ENTER:\n");
    getchar();
    while(testa_fim(computador) != 1 || testa_fim(quem_joga) != 1) {
        printf("\e[2J\e[H");
        printf("comandante %s, o que deseja fazer?\n", PLAYER.nome);
        printf("\t1 - Mostrar Seu Mapa  \n\t2 - Mostra Mapa do Atacante\n\t3 - Atacar\n");
        escolha = getchar();
        getchar();
        switch(escolha){
            case '1':
                printf("TABULEIRO JOGADOR\n",computador,*computador);
                imprime_tabuleiro(quem_joga);
                printf("Pressione ENTER:\n");
                getchar();
                break;
            case '2':
                printf("TABULEIRO COMPUTADOR\n");
                imprime_tabuleiro_computador(computador);
                printf("Pressione ENTER:\n");
                getchar();
                break;
            case '3':
                valor = tiro_jogador(computador);
                ataque_computador(quem_joga,ultimo_ataque_computador,sucesso_ultimo_ataque,sentido_direcao);
                printf("Pressione ENTER\n");
                getchar();
        }
    }
    if(testa_fim(computador) != 1){
      printf("DERROTAMOS NOSSO INIMIGO!!!!!!!!\n É UM DIA DE ALEGRIA PARA TODOS NÓS!!!!!");
      getchar();
      printf("\e[2J\e[H");
      printf("Parabéns %s sabíamos que você iria nos salvar dos repressores, é uma honra ter tão incrível estrategista entre nós. URRA!!!!\n", PLAYER.nome);
      printf("Pressione ENTER para encerrar.\n");
    }
    else if(testa_fim(quem_joga) != 1){
      printf("OH NÃO... AFUNDARAM NOSSO ULTIMO NAVIO...\nInfelizmente fracassamos dessa vez, na próxima não deixaremos barato!");
      getchar();
      printf("\e[2J\e[H");
      printf("VOCÊ PERDEU, pressione ENTER para voltar ao terminal\n");
      getchar();
    }
    return 0;

}
